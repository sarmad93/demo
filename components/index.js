import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    StatusBar,
    Platform,
    Dimensions,
    Button,
} from 'react-native';

import Draggable from './Draggable';
import DialogWindow from './DialogWindow.js';


import PopupDialog, { DefaultAnimation } from 'react-native-popup-dialog';
import { StackNavigator } from 'react-navigation';



export default class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            dropZoneValues: [],
            dropOnTargetDetails: [],
            draggableIds: [],
            dialogShow: false,
        };

    }

    static navigationOptions = {
        title: 'Welcome',
    };

    isDropZone(gesture, type) {
        //console.log("gesture", gesture);
        console.log("DropTarget " + JSON.stringify(this.state.dropOnTargetDetails));
        let dz = this.state.dropZoneValues;
        let dzValues;


        var isDropZone = false;

        dropdetails = this.state.dropOnTargetDetails;

        /* dropdetails.map((data) => {
 
 
         })*/

        dz.map((dz, index) => {
            // console.log("afterDrag", dz)
            if ((gesture.moveX > dz.x && gesture.moveX < dz.x + dz.width) && (gesture.moveY > dz.y && gesture.moveY < dz.y + dz.height)) {
                //  console.log("afterDrag")
                // isDropZone = true;
                //dzValues = dz;

                //  alert(type);
                //  alert(this.state.dropOnTargetDetails[index].type);
                if (this.state.dropOnTargetDetails[index].type === type) {
                    // alert("matched");
                    isDropZone = true;
                    dzValues = dz;
                }

            }

        })

        // console.log("dzvalues" + dzValues.y)
        return { isDropZone, dzValues };
    }



    setDropZoneValues(type, event, value) {
        // console.log("value", value);
        details = {}
        details[value] = event.nativeEvent.layout;
        details["type"] = type;
        this.setState({
            dropZoneValues: this.state.dropZoneValues.concat(event.nativeEvent.layout),
            dropOnTargetDetails: this.state.dropOnTargetDetails.concat(details)
        });

        // console.log("DropTarget " + JSON.stringify(this.state.dropZoneValues));
    }


    render() {

        return (

            <View style={styles.mainContainer}>

                <View style={styles.dropZone}>

                    <View style={styles.box1} onLayout={(e) => this.setDropZoneValues(1, e, null)} >
                        <View style={styles.borderBox} />
                    </View>
                    <View style={styles.box2} onLayout={(e) => this.setDropZoneValues(2, e, null)} >
                        <View style={styles.borderBox} />
                    </View>
                    <View style={styles.box3} onLayout={(e) => this.setDropZoneValues(3, e, null)} >
                        <View style={styles.borderBox} />
                    </View>
                    <View style={styles.box4} onLayout={(e) => this.setDropZoneValues(4, e, null)} >
                        <View style={styles.borderBox} />
                    </View>
                </View>

                <View style={styles.panZone}>
                    {this.renderDraggable()}
                </View>

                <DialogWindow show={this.state.dialogShow} onDismissed={() => this.setState({dialogShow: false})} />

            </View >
        );
    }


    renderDraggable() {


        const components = []
        console.log(this.props);

        for (var i = 1; i <= 2; i++) {
            if (!this.state['hideDraggable_' + i]) {
                let index = i;
                components.push(
                    <Draggable key={i} x={250} y={50 + i * 80} onDrop={(pan, gesture) => this.onDrop(index, pan, gesture, this.props)} />
                );
            }
        }

        return components;
    }

    onDrop(index, pan, gesture, props) {
        // const { navigate } = props.navigation;
        let details = this.isDropZone(gesture, index);

        if (details.isDropZone) {

            this.setState({
                ['hideDraggable_' + index]: true,
                dialogShow: true,

            });

            return true;
        }
    }
}




let CIRCLE_RADIUS = 12;
let Window = Dimensions.get('window');
let styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        flexDirection: 'row'
    },
    dropZone: {
        //height: 100,
        flex: 1,
        // backgroundColor: '#2c3e50',

        // width:50,

        flexDirection: 'column',
        justifyContent: 'space-around',
        // alignItems:'center',
        marginLeft: 10,
        marginRight: 10,
        // backgroundColor:'blue'
    },
    text: {
        marginTop: 25,
        marginLeft: 5,
        marginRight: 5,
        textAlign: 'center',
        color: '#fff'
    },
    draggableContainer: {
        //flexDirection:'column',
        position: 'absolute'
    },
    circle1: {
        backgroundColor: '#1abc9c',
        width: CIRCLE_RADIUS * 1.5,
        height: CIRCLE_RADIUS * 1.5,
        borderRadius: CIRCLE_RADIUS
    },
    circle2: {
        backgroundColor: 'red',
        width: CIRCLE_RADIUS * 1.5,
        height: CIRCLE_RADIUS * 1.5,
        borderRadius: CIRCLE_RADIUS
    },
    box1: {

        width: 150,
        height: 50,
        backgroundColor: '#FF8A80'
    },

    box2: {


        width: 150,
        height: 50,
        backgroundColor: '#FF8A80'
    },

    box3: {

        width: 150,
        height: 50,
        backgroundColor: '#FF8A80'
    },
    box4: {
        width: 150,
        height: 50,
        backgroundColor: '#FF8A80'
    },
    panZone: {
        position: 'absolute',
        left: 0,
        top: 0
        //backgroundColor: 'red'

    },
    borderBox: {
        width: 150,
        height: 5,
        position: 'absolute',
        left: 0,
        bottom: 0,
        backgroundColor: 'black'
    }
});

/*export default App = StackNavigator({
    Home: {
        screen: HomeScreen,
        //headerMode: 'screen',
    },
    Matched: {
        screen: MatchedScreen,
        headerMode: 'screen',
    },
},
    {
        initialRouteName: 'Home',
        headerMode: 'float',
    }
);*/



/*const styles = StyleSheet.create({
                    container: {
                    flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
                    fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
                    textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});*/



  /*   <View key={0} style={styles.draggableContainer} ref="green" >

                    <Animated.View
                        {...this.panResponder_1.panHandlers}
                        style={[this.state.pan_1.getLayout(), styles.circle1]}>

                    </Animated.View>
                </View>*/