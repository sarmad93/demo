import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Text,
    Button
} from 'react-native';

import PropTypes from 'prop-types';

import PopupDialog, { DefaultAnimation, DialogTitle } from 'react-native-popup-dialog';

const defaultAnimation = new DefaultAnimation({ animationDuration: 150 });


export default class DialogWindow extends Component {

    constructor(props) {
        super(props);
        //this.showDefaultAnimationDialog = this.showDefaultAnimationDialog.bind(this);
    }

    render() {
        return (
            <PopupDialog
                width={300}
                height={300}
                ref='popupDialog'
                show={this.props.show}
                onDismissed={this.props.onDismissed}
                dialogTitle={<DialogTitle title="Popup Dialog - Default Animation" />}>
                <View>
                    <Text>Test</Text>
                </View>
            </PopupDialog>
        )
    }

}

/*
let styles = StyleSheet.create({

    container: {
        f
        alignItems: 'center',
        justifyContent: 'center',
    },
})*/