import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    StatusBar,
    Platform,
    Dimensions,
    Animated,
    PanResponder,

} from 'react-native';



import MatchedScreen from './MatchedScreen'
import PopupDialog, { DefaultAnimation } from 'react-native-popup-dialog';

const defaultAnimation = new DefaultAnimation({ animationDuration: 150 });



export default class Draggable extends Component {

    constructor(props) {
        super(props);

       // this.showDefaultAnimationDialog = this.showDefaultAnimationDialog.bind(this);
       // this.openDialogWindow = this.openDialogWindow.bind(this);

        const pan = new Animated.ValueXY({ x: 0, y: 0 });

        this.panResponder = PanResponder.create({

            onMoveShouldSetResponderCapture: () => true,
            onStartShouldSetPanResponderCapture: () => true,

            onPanResponderGrant: (evt, gestureState) => {
                gestureState.dx = this.props.x || 0;
                gestureState.dy = this.props.y || 0;
                console.log("eventIdPan1 " + evt.nativeEvent.target);
            },
            onPanResponderMove: Animated.event([null, {
                dx: pan.x,
                dy: pan.y
            }]),
            onPanResponderRelease: (e, gesture) => {

                const dropHandled = this.props.onDrop && this.props.onDrop(this.state.pan, gesture);

                if (!dropHandled) {
                    Animated.spring(
                        this.state.pan,
                        { toValue: { x: this.props.x, y: this.props.y } }
                    ).start();

                   // this.defaultAnimationDialog();
                }
            }

        });

        this.state = { pan };
    }

    componentDidMount() {
        const x = this.props.x || 0;
        const y = this.props.y || 0;

        this.state.pan.setValue({ x, y });
    }

    render() {

        return (
            <Animated.View
                {...this.panResponder.panHandlers}
                style={[this.state.pan.getLayout(), styles.circle]}>
            </Animated.View>
            );
    }
}





let CIRCLE_RADIUS = 20;
let Window = Dimensions.get('window');

let styles = StyleSheet.create({
    circle: {
        backgroundColor: '#1abc9c',
        width: CIRCLE_RADIUS * 2,
        height: CIRCLE_RADIUS * 2,
        borderRadius: CIRCLE_RADIUS,
        position: 'absolute'
    },

    container: {
        
        alignItems: 'center',
        justifyContent: 'center',
    },

});