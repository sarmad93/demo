/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import {
  AppRegistry,
  
} from 'react-native';

import App from './components/index';

AppRegistry.registerComponent('Demo', () => App);
